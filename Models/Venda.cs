
using System.ComponentModel.DataAnnotations;
using tech_test_payment_api.Dtos;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        [Key]
        public int id { get; set; }

        public DateTime data { get; set; }

        public Vendedor vendedor { get; set; }

        public HashSet<Produto> produtos { get; set; }

        public Status status { get; set; }

        public Venda(VendaDto vendaDto)
        {
            data = vendaDto.data;
            vendedor = vendaDto.vendedor;
            produtos = new HashSet<Produto>(vendaDto.produtos);
            status = Status.Aguardando;
        }
        public Venda()
        {
            
        }
    }
}