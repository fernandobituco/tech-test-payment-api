namespace tech_test_payment_api.Models
{
    public enum Status
    {
        Aguardando,
        Aprovado,
        Enviado,
        Entregue,
        Cancelado
    }
}