
using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models
{
    public class Vendedor
    {
        [Key]
        public int Id { get; set; }

        public string Cpf { get; set; }

        public string Nome { get; set; }

        public string Email { get; set; }

        public string Telefone { get; set; }
    }
}