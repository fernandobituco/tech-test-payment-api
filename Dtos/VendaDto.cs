using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Dtos
{
    public class VendaDto
    {
        public DateTime data { get; set; }

        public Vendedor vendedor { get; set; }

        public HashSet<Produto> produtos { get; set; }
    }
}