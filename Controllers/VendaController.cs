using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Contexts;
using tech_test_payment_api.Dtos;
using tech_test_payment_api.Exceptions;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendasContext _vendasContext;

        public VendaController(VendasContext vendasContext)
        {
            _vendasContext = vendasContext;
        }

        [HttpGet]
        public IActionResult LerTodos()
        {
            var vendas = _vendasContext.Vendas;
            if (vendas == null)
            {
                return NotFound("Não há vendas registradas");
            }

            return Ok(vendas);
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult LerPorId(int id)
        {
            Venda venda = new Venda();
            try
            {
                venda = BuscarPorId(id);
            } catch(VendaNotFoundException e)
            {
                return NotFound(e);
            }

            return Ok(venda);
        }


        [HttpPost]
        public IActionResult Criar(VendaDto vendaDto)
        {
            Venda venda = new Venda(vendaDto);
            _vendasContext.Add(venda);
            _vendasContext.SaveChanges();
            return Created(nameof(LerPorId), venda);
        }

        [HttpPut]
        public IActionResult Atualizar(int id, Status status)
        {
            Venda venda = new Venda();
            try
            {
                venda = BuscarPorId(id);
            } catch(VendaNotFoundException e)
            {
                return NotFound(e);
            }

            if ((int)status - (int)venda.status != 1 && status != Status.Cancelado)
            {
                return BadRequest("Essa mudança de status não é permitida");
            }
            
            venda.status = status;
            _vendasContext.SaveChanges();
            return Ok();
        }

        private Venda BuscarPorId(int id)
        {
            Venda venda = _vendasContext.Vendas.Find(id);
            if(venda == null)
            {
                throw new VendaNotFoundException("Venda não encontrada");
            }

            return venda;
        }
    }
}