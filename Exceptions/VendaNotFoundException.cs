using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Exceptions
{
    public class VendaNotFoundException : Exception
    {
        public VendaNotFoundException(string message) : base(message)
        {
            
        }
    }
}